import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import * as routes from "./routes";

// initialize configuration
dotenv.config();

// dotenv config
const port = process.env.SERVER_PORT || 8080;

const app = express();

// adding cors
app.use(cors());

// add body parser
app.use(express.json());

// Configure routes
routes.register( app );

// start the express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );