import * as express from "express";
import * as techRoutes from "../api/technology";
import * as ringPlacementHistoryRoutes from "../api/technology.history";
import * as ringRoutes from "../api/ring";
import * as catRoutes from "../api/category";

const register = ( app: express.Application ) => {
    // define a route handler for the default home page
    app.get( "/", ( req: any, res ) => {
        res.send( "Hello world!" );
    } );

    techRoutes.register(app);
    ringRoutes.register(app);
    catRoutes.register(app);
    ringPlacementHistoryRoutes.register(app);
};

export { register };