import { PrismaClient } from '@prisma/client';
import * as testData from './testData.json';

const prisma = new PrismaClient();

(async () => {
	// add rings
	const ringsPromise = prisma.ring.createMany({
		data: [
			{ id: 1, name: 'Hold' },
			{ id: 2, name: 'Assess' },
			{ id: 3, name: 'Trial' },
			{ id: 4, name: 'Adopt' }
		],
		skipDuplicates: true
	});

	// add categories
	const categoriesPromise = prisma.category.createMany({
		data: [
			{ id: 1, name: 'Techniques' },
			{ id: 2, name: 'Tools' },
			{ id: 3, name: 'Platforms' },
			{ id: 4, name: 'Frameworks' }
		],
		skipDuplicates: true
	});

	await Promise.all([ringsPromise, categoriesPromise]).then(([rings, categories]) => {
		console.group("initial data import");
		console.log("rings: ", rings);
		console.log("categories: ", categories);
		console.groupEnd();
	});

	// import test data in development mode
	if (process.env.NODE_ENV === 'dev'){
		const technologiesPromise = prisma.technology.createMany({
			data: testData.technologies,
			skipDuplicates: true
		});

		const ringPlacementHistoryPromise = prisma.ringPlacementHistory.createMany({
			data: testData.ringPlacementHistory,
			skipDuplicates: true
		});

		prisma.$transaction([technologiesPromise, ringPlacementHistoryPromise]).then(([ technologies, ringPlacementHistory ]) => {
			console.group("test data import");
			console.log("technologies: ", technologies);
			console.log("ringPlacementHistory: ", ringPlacementHistory);
			console.groupEnd();
		});
	}
})();