import * as express from "express";
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const register = ( app: express.Application ) => {
	app.get( "/ring", async ( req, res ) => {
		const rings = await prisma.ring.findMany();

		res.json(rings);
	} );

	app.get( "/ring/:id", async ( req, res ) => {
		const ringId = parseInt(req.params.id, 10) || 0;

		const ring = await prisma.ring.findUnique({
			where: {
				id: ringId
			}
		});

		if (ring){
			res.json(ring);
		}else{
			res.status(404).send('Ring not found');
		}

	} );
};

export { register };