import * as express from "express";
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const register = ( app: express.Application ) => {
	app.get( "/category", async ( req, res ) => {
		const categories = await prisma.category.findMany();

		res.json(categories);
	} );

	app.get( "/category/:id", async ( req, res ) => {
		const categoryId = parseInt(req.params.id, 10) || 0;

		const category = await prisma.category.findUnique({
			where: {
				id: categoryId
			}
		});

		if (category){
			res.json(category);
		}else{
			res.status(404).send('Category not found');
		}

	} );
};

export { register };