import * as express from "express";
import { Prisma, PrismaClient } from '@prisma/client';
import { checkTechnologyExistsById } from './technology.utils';

const prisma = new PrismaClient();

const allowedExpandFields = ["newRing", "oldRing"];

const register = ( app: express.Application ) => {
	app.get( "/technology/:id/history", async ( req, res ) => {
		const technologyId = parseInt(req.params.id, 10) || 0;

		if (!(await checkTechnologyExistsById(technologyId))){
			res.status(404).send('Technology not found');
			return;
		}

		// filter
		const expand = req.query.expand;

		const queryOptions: {
			include?: Prisma.RingPlacementHistoryInclude,
			where?: Prisma.RingPlacementHistoryWhereInput,
			orderBy?: Prisma.Enumerable<Prisma.RingPlacementHistoryOrderByWithRelationInput>
		} = {
			where: {
				technologyId
			},
			orderBy: [{
				id: 'desc'
			}]
		};

		await prisma.technology.findMany()

		if (typeof expand === "string"){
			// get valid expand fields
			const expandFields = expand.split(",").filter(field => allowedExpandFields.includes(field));

			if (expandFields.length > 0){
				queryOptions.include = {
					newRing: expandFields.includes("newRing"),
					oldRing: expandFields.includes("oldRing")
				};
			}
		}

		const technologies = await prisma.ringPlacementHistory.findMany(queryOptions);

		res.json(technologies);
	} );
};

export { register };