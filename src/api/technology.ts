import * as express from "express";
import { Prisma, PrismaClient } from '@prisma/client';
import { checkTechnologyExistsById, getTechnologyById, isObjectValidForCreateOrUpdate, isObjectValidForRingPlacementChange } from './technology.utils';

const prisma = new PrismaClient();

const allowedExpandFields = ["category", "ring"];

const register = ( app: express.Application ) => {
	app.get( "/technology", async ( req, res ) => {
		// filter
		const published = req.query.published;
		const expand = req.query.expand;

		const queryOptions: {
			include?: Prisma.TechnologyInclude,
			where?: Prisma.TechnologyWhereInput
		} = {};

		if (published !== undefined){
			queryOptions.where = {
				published: !!parseInt(published as string, 10)
			};
		}

		if (typeof expand === "string"){
			// get valid expand fields
			const expandFields = expand.split(",").filter(field => allowedExpandFields.includes(field));

			if (expandFields.length > 0){
				queryOptions.include = {
					category: expandFields.includes("category"),
					ring: expandFields.includes("ring")
				};
			}
		}

		const technologies = await prisma.technology.findMany(queryOptions);

		res.json(technologies);
	} );

	app.get( "/technology/:id", async ( req, res ) => {
		const technologyId = parseInt(req.params.id, 10) || 0;

		const technology = await prisma.technology.findUnique({
			where: {
				id: technologyId
			}
		});

		if (technology){
			res.json(technology);
		}else{
			res.status(404).send('Technology not found');
		}
	} );

	app.post( "/technology", async ( req, res ) => {
		if (!isObjectValidForCreateOrUpdate(req.body)){
			res.status(400).send('Invalid body data');
			return;
		}

		const technology = req.body;

		// if id is set, check if record already exists
		if (technology.id){
			// if already exists, throw error
			if (await checkTechnologyExistsById(technology.id)){
				res.status(409).send('Technology with given id already exists');
				return;
			}
		}

		const newTechnology = await prisma.technology.create({
			data: technology
		});

		res.json(newTechnology);
	} );

	app.put( "/technology/:id", async ( req, res, next ) => {
		const technology = req.body;

		if (!isObjectValidForCreateOrUpdate(technology)){
			next();
			return;
		}

		const technologyId = parseInt(req.params.id, 10) || 0;

		if (!(await checkTechnologyExistsById(technologyId))){
			res.status(404).send('Technology not found');
			return;
		}

		// make sure the id isn't being overridden
		delete technology.id

		const updatedTechnology = await prisma.technology.update({
			where: {
				id: technologyId
			},
			data: technology
		});

		if (updatedTechnology){
			res.json(updatedTechnology);
		}else{
			res.status(404).send('Technology not found');
		}
	} );

	app.put( "/technology/:id", async ( req, res ) => {
		const technology = req.body;

		if (!isObjectValidForRingPlacementChange(technology)){
			res.status(400).send('Invalid body data');
			return;
		}

		const technologyId = parseInt(req.params.id, 10) || 0;

		const oldTechnology = await getTechnologyById(technologyId);

		if (oldTechnology === null){
			res.status(404).send('Technology not found');
			return;
		}

		const updatedTechnology = await prisma.technology.update({
			where: {
				id: technologyId
			},
			data: {
				ringId: technology.ringId,
				ringPlacementDescription: technology.ringPlacementDescription,
				ringPlacementHistory: {
					create: [{
						oldRingId: oldTechnology.ringId,
						oldRingPlacementDescription: oldTechnology.ringPlacementDescription,
						newRingId: technology.ringId,
						newRingPlacementDescription: technology.ringPlacementDescription
					}]
				}
			}
		})

		if (updatedTechnology){
			res.json(updatedTechnology);
		}else{
			res.status(404).send('Technology not found');
		}
	} );

	app.put( "/technology/:id/publish", async ( req, res ) => {
		const technology = req.body;
		const technologyId = parseInt(req.params.id, 10) || 0;

		if (!(await checkTechnologyExistsById(technologyId))){
			res.status(404).send('Technology not found');
			return;
		}

		const updatedTechnology = await prisma.technology.update({
			where: {
				id: technologyId
			},
			data: {
				published: true,
				publishedAt: new Date(),
				ringId: technology.ringId,
				ringPlacementDescription: technology.ringPlacementDescription
			}
		});

		if (updatedTechnology){
			res.json(updatedTechnology);
		}else{
			res.status(404).send('Technology not found');
		}
	} );

	app.delete( "/technology/:id", async ( req, res ) => {
		const technologyId = parseInt(req.params.id, 10) || 0;

		if (await checkTechnologyExistsById(technologyId)){
			await prisma.technology.delete({
				where: {
					id: technologyId
				}
			});

			res.status(200).send();
		}else{
			res.status(200).send();
		}
	} );
};

export { register };