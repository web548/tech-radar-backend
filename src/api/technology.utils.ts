import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const checkTechnologyExistsById = async (id: number) => {
	const technology = await prisma.technology.findUnique({
		where: {
			id
		}
	});

	return technology !== null;
}

const getTechnologyById = (id: number) => {
	return prisma.technology.findUnique({
		where: {
			id
		}
	});
}

const isObjectValidForCreateOrUpdate = (obj: any) => {
	const keys = Object.keys(obj);
	const keysToCheck = ["name", "categoryId", "description"];

	return keysToCheck.every(el => keys.includes(el));
}

const isObjectValidForRingPlacementChange = (obj: any) => {
	const keys = Object.keys(obj);
	const keysToCheck = ["ringId", "ringPlacementDescription"];

	return keysToCheck.every(el => keys.includes(el));
}

export { checkTechnologyExistsById, getTechnologyById, isObjectValidForCreateOrUpdate, isObjectValidForRingPlacementChange }