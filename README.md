# WEBLAB Technology-Radar Backend (Express)


## Prerequisites
### Database
Before running the project, a database must be created and an `.env` file should be created with the database information.
We will be using a MySQL docker image. To do this, docker must first be installed - if this is not the case, you can follow this [guide](https://docs.docker.com/get-docker/).
Pulling the MySQL image:
```
docker pull mysql:latest
```

Starting the MySQL instance:
```
docker run --name weblab-db -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_DATABASE=weblab -d -p 3306:3306 mysql
```

Example MySQL configuration in `.env` file:
```
# MySQL configuration
DB_HOST=localhost
DB_USER=root
DB_DATABASE=weblab
DB_PASSWORD=my-secret-pw
DB_PORT=3306
DB_URL=mysql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}
```

### Server Configuration
If you wish to use another server port other than the default 8080, then one should be defined in the `.env` file as such:
```
SERVER_PORT=8080
```

### npm scripts
1. Install dependencies: `npm install`
2. Initialize the database with prisma: `npm run initdb`

## Running the project
Run: `npm start`

## Development mode with live-reload
Run: `npm run dev`